<?php

$iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator(__DIR__));

/** @var SplFileInfo $file */
foreach ($iterator as $file) {
    $filePath = $file->getRealPath();
    if ($file->getFilename() === 'update.php' || $file->isDir()) continue;

    // Get controller file content and find validation rules
    $controllerContent = file_get_contents($filePath);
    $matches = [];
    preg_match_all('/=>\s*([\'"].*?\|.*?[\'"])[\s,]*\n/', $controllerContent, $matches);

    // Update rule blocks
    foreach ($matches[1] as $validationString) {
        $ruleList = array_map(function($rule) {
            return "'" . $rule . "'";
        }, explode('|', trim($validationString, '\'"')));
        $newCode = '[' . (implode(', ', $ruleList)) . ']';
        $controllerContent = str_replace($validationString, $newCode, $controllerContent);
    }

    file_put_contents($filePath, $controllerContent);
}